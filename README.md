Plate PA 4
Lane Koistinen
06.15.2020

Usage: [program name] [database file (.txt)]

Program accepts a database of license plates to construct a custom binary search tree.

All allocated memory is freed.

Supported commands (via stdin):
    *DUMP           = print entire database in alphabetical order
    *DELETE [plate] = delete plate with entered name
    
Makefile included.