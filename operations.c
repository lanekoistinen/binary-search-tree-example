/////////////////////////////////////////////
//
//  LANE KOISTINEN
//  2.27.2020
//  BST Tree operations
//
//  Operations performs the most basic
//  functions on a binary search tree
//  
////////////////////////////////////////////



#include "pafour.h"



///////////////////////////////
//	add
//
//  add inserts a new node
//  in the appropriate part
//  of the tree
//  Inputs: The root and plate
//  information
//  Outputs: the root node

Node add(Node root, char *plate, char *first, char *last)
{
        // Add a node if the root is NULL
	if(root == NULL)
                return(newNode(plate, first, last));

	// If the plate is keyed less than root, add to left
        if(strcmp(plate,root->plate) < 0)
                root->left = add(root -> left, plate, first, last);
        else
        // If keyed as more than root, add to right
                root-> right = add(root->right, plate, first, last);
        return root;
}



//////////////////////////////
//	newNode
//
//  newNode mallocs a new node
//  and copies in the relevant
//  data
//  Inputs: the plate information
//  Outputs: the new node pointer

Node newNode(char *plate, char *first, char *last)
{
	// malloc plate memory
        Node new = malloc(sizeof(struct node));
        new->plate = malloc(strlen(plate) + 1);
        new->first = malloc(strlen(first) + 1);
        new->last  = malloc(strlen(last)  + 1);
        new->left  = NULL;
        new->right = NULL;

	// Copy plate info
        strcpy(new->plate, plate);
        strcpy(new->first, first);
        strcpy(new->last, last);
        return new;
}



///////////////////////////////////
//	LNR
//
//  LNR traverses the tree in order:
//  left node right
//  Input: root node
//  Outputs: none

void LNR(Node root)
{
        if (root == NULL)
                return;

	// Traverse left
        LNR(root->left);
	
	// Print node
        printf("Plate: <%s> Name: %s, %s\n", root->plate, root->last, root->first);
        
	// Traverse right
	LNR(root->right);

        return;

}



//////////////////////////////////
//	NLR
//
//  NLR traverses the tree in order:
//  node left right
//  Input: root node
//  Outputs: none

void NLR(Node root)
{
        if (root == NULL)
                return;

	// Print node
        printf("Plate: <%s> Name: %s, %s\n", root->plate, root->last, root->first);
       
	// Traverse left
	NLR(root->left);

	// Traverse right
        NLR(root->right);
}



/////////////////////////////////
//	LRN
//
//  LRN traverses the tree in order:
//  left right node
//  Input: root node
//  Outputs: none

void LRN(Node root)
{
        if (root == NULL)
                return;

	// Traverse left
        LRN(root->left);

	// Traverse right
        LRN(root->right);

	// Print node
        printf("Plate: <%s> Name: %s, %s\n", root->plate, root->last, root->first);
}



/////////////////////////////////
//	height
//
//  height calculates the height,
//  or depth, of a given tree
//  Inputs: root node
//  Outputs: height of tree

int height(Node root)
{
	// Here is the base case: return a height of -1
        if (root == NULL)
                return -1;
        int hLeft = height(root->left);
        int hRight = height(root->right);

	// Return max of left or right
	// (one more for moving up level)
        if (hLeft > hRight)
                return hLeft + 1;
        else
                return hRight + 1;
}



//////////////////////////////////
//	delete
//
//  delete node takes a root and plate
//  and deletes the node's plate when found
//  Inputs: root node and plate
//  Outputs: the root node

Node delete(Node root, char *plate)
{
	// If the plate matches, delete
        if (strcmp(root->plate, plate) == 0)
        {
                Node newRoot;
                
		// If there is a left tree...
		if(root->left != NULL)
                {
			// Find the left largest and make it the node
                        struct leftLargestReturn leftLargest = findLeftLargest(root->left);
                        leftLargest.node->right = root->right;

			// Have the prior node adopt the left largest child
                        if(leftLargest.prior != NULL)
                        {
                                leftLargest.prior->right = leftLargest.node->left;
                                leftLargest.node->left = root->left;
                        }

			// Free root
                        freeNode(root);
                        newRoot = leftLargest.node;
                }
                else
                {
			// Otherwise, make the new root the old root's right tree
                        newRoot = root->right;
			freeNode(root);
                }
                return newRoot;
        }

	// If the plate does not match, delete from the relevant side of the tree (using key)
        if(strcmp(plate, root->plate) < 0)
                root->left = delete(root->left, plate);
        else
                root->right = delete(root->right, plate);
        return root;
}



//////////////////////////////////
//	leftLargestReturn
//
//  leftLargestReturn finds the pointer
//  to largest node in the left subtree
//  Inputs: root node of subtree
//  Output: returns a structure with the 
//  left largerest node and its prior node

struct leftLargestReturn findLeftLargest(Node root)
{
        struct leftLargestReturn returnMe;

	// If the current node has no right
	// return the current node, this is a base case
        Node curr = root;
        if(curr->right == NULL)
        {
                returnMe.prior = NULL;
                returnMe.node = curr;
                return returnMe;
        }

	// Normally we search right right node as follows,
	// this helps us save the prior node
        if(curr->right->right == NULL)
        {
                returnMe.prior = curr;
                returnMe.node = curr->right;
                return returnMe;
        }

	// If this point, is reached go deeper
	// into our recursion
        returnMe = findLeftLargest(curr->right);
        return returnMe;
}



////////////////////////////////////
//	balanced
//
//  balanced tests various conditions
//  to see if the tree is balanced
//  Inputs: tree root
//  Outputs: 1 is balanced, 0
//  if not balanced

int balanced(Node root)
{
	// If there is no tree, we consider it balanced
        if (root == NULL)
                return 1;

	// Check the following three conditions to see if 
	// the tree is balanced
        if( balanced(root->left) == 1
         && balanced(root->right) == 1
         && abs(height(root->left) - height(root->right)) <= 1)
                return 1;
        else return 0;
}



//////////////////////////////////
//	search
//
//  search traverses tree in search
//  of node. If it finds the node,
//  first and last variables are
//  loaded with the info
//  Inputs: root note, plate, and
//  memory space for first and last
//  names
//  Outputs: 1 if found and loaded,
//  0 if not found

int search(Node root, char* plate, char* first, char* last)
{
        if(root == NULL)
                return 0;

	// If found, load data and return
        if (strcmp(root->plate, plate) == 0)
        {
                strcpy(first, root->first);
                strcpy(last, root->last);
                return 1;
        }

	// Search left and right
        if(search(root->left, plate, first, last) == 1)
                return 1;
        if(search(root->right, plate, first, last) == 1)
                return 1;
        return 0;
}



//////////////////////////////////
//	treeFree
//
//  treeFree frees all elements
//  of a given tree
//  Input: root node
//  Output: none

void treeFree(Node root)
{
        if(root == NULL)
                return;
        treeFree(root->left);
        treeFree(root->right);
        freeNode(root);
        return;
}



//////////////////////////////////
//	freeNode
//
//  freeNode frees an individual
//  node
//  Inputs: node
//  Outputs: none

void freeNode(Node freeMe)
{
        free(freeMe->plate);
        free(freeMe->first);
        free(freeMe->last);
        free(freeMe);
}
    
