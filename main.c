/////////////////////////////////////////////
//
//  LANE KOISTINEN
//  2.27.2020
//  BST Process input from Cmd line
//
//  Main calls subfunctions to manipulate
//  a binary search tree
//
////////////////////////////////////////////




#include "pafour.h"



/////////////////////////////////
//	main
//
//  main recieves user input
//  and calls various commands
//  Inputs: recieves database 
//  from command line
//  Outputs: 1 if error, 0 
//  if success

int main(int argc, char **argv)
{
	Node root = NULL;

        // Check for error in command line
        if (argc != 2)
        {
                printf("Usage: %s database\n", argv[0]);
                return 0;
        }

        // Open the file and read from the database
        FILE *fp = fopen(argv[1], "r");
        if (fp == NULL)
        {
                printf("File Opening Error\n");
                return 1;
        }

	// Retrieve each line
        char temp[256];
        while (fgets(temp, 256, fp) != NULL)
        {
                char plate[32], first[32], last[32];
                sscanf(temp, "%s %s %s\n", plate, first, last);

                // Add each plate from the database
		root = add(root, plate, first, last);
        }
        fclose(fp);

        char *nullTest;
        do {
                printf("Enter command or plate: ");

                // Declare variables
                int cmd = 3;
                char* plateDelete = malloc(130);
                char* testChar = malloc(130);
                char commandInput[100];
		char* first = malloc(130);
		char* last = malloc(130);
		strcpy(testChar,"xxx null flag xxx");

		// Get user input
                nullTest = fgets(commandInput, 100, stdin);

                if (nullTest != NULL) // Stop if NULL input
                {
			// Scan for each different commands and adjust the
                        // result command according to findings
                        sscanf(commandInput, "%s\n", testChar);

			int result = strcmp(testChar, "*DUMP");
                        if (result == 0)
                                cmd = 0;

			// Peform delete here if applicable
                        result = sscanf(commandInput, "*DELETE %s", plateDelete);
                        if (result == 1)
			{
                  		if (search(root, plateDelete, first, last) == 1)
		                {	
					root = delete(root, plateDelete);
					printf("SUCCESS\n");
				}
				else
					printf("PLATE NOT FOUND\n");
		 		cmd = 1;
			}

                        // Issue command
                        command(root, cmd, plateDelete, testChar);
                }
		free(plateDelete); free(testChar); free(first); free(last);
        } while (nullTest != NULL); // this indicates EOF is reached. Program is free to terminate

        // Free all memory
        printf("\nFreeing memory\n");
	treeFree(root);
        return 0;
}



////////////////////////////////
//	command
//
//  command recieves the command 
//  from main and calls the 
//  appropriate subfunctions

int command(Node root, int cmd, char* plateDelete, char* testChar)
{
        char* first = malloc(130);
        char* last =  malloc(130);

        // Determine the requested command
        switch (cmd)
        {
        // Dump
        case 0:
		// Dump tree height and balance
		printf("TREE HEIGHT: %d\n", height(root));
		if (balanced(root) == 1)
			printf("BALANCED: YES\n");
		else
			printf("BALANCED: NO\n");
		
		// Traverse each way
		printf("\nLNR TRAVERSAL:\n");
		LNR(root);
		printf("\nNLR TRAVERSAL:\n");
		NLR(root);
		printf("\nLRN TRAVERSAL:\n");
		LRN(root);
        break;

        // Find a plate
        case 3:
		if(testChar != NULL)
		{
 			if(search(root, testChar, first, last) == 1)
				printf("First name: %s\nLast name: %s\n", first, last);
			else
				printf("PLATE NOT FOUND\n");
		}
		else
			printf("PLATE NOT FOUND\n");

        break;
        default:break;
        }
        // Free retrieval variables, first and last
        free(first); free(last); 
        return 0;
}



