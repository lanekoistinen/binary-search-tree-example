/////////////////////////////////////////////
//
//  LANE KOISTINEN
//  2.27.2020
//  BST Header
//
//  Contains function delcarations for both
//  main and the subfunctions in operations
//
////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct node{
	char *plate;
	char *first;
	char *last;
	struct node *left;
	struct node *right;
};

typedef struct node* Node;

struct leftLargestReturn{
        Node prior;
        Node node;
};

Node add(Node root, char *plate, char *first, char *last);
Node newNode(char *plate, char* first, char* last);
void LNR(Node root);
void NLR(Node root);
int height(Node root);
Node delete(Node root, char *plate);
struct leftLargestReturn findLeftLargest(Node root);
int balanced(Node root);
int search(Node root, char* plate, char* first, char* last);
void LRN(Node root);
void freeNode(Node freeMe);
void treeFree(Node root);
int command(Node root, int cmd, char* plateDelete, char* testChar);

